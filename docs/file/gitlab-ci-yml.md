# .gitlab-ci.yml

```yml
image: python:3.9-slim  # Adjust Python version if needed

stages:
  - build
  - deploy

build:
  stage: build
  script:
    - pip install mkdocs
    - mkdocs build  # Build the mkdocs documentation
  artifacts:
    paths:
      - site  # Replace with your mkdocs output directory if different

deploy:
  stage: deploy
  script:
    - AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY aws s3 sync --delete site s3://your-bucket-name/  # Replace with your S3 bucket name
  only:
    - master  # Deploy only on pushes to the master branch (adjust as needed)

cache:
  paths:
    - ~/.cache/pip  # Cache pip dependencies to speed up builds
```

<a href="#download-gitlab-ci-yml" id="download-gitlab-ci-yml">Download Yaml File</a>
<textarea id="gitlab-ci-yml" class="d-none">
image: python:3.9-slim  # Adjust Python version if needed

stages:
  - build
  - deploy

build:
  stage: build
  script:
    - pip install mkdocs
    - mkdocs build  # Build the mkdocs documentation
  artifacts:
    paths:
      - site  # Replace with your mkdocs output directory if different

deploy:
  stage: deploy
  script:
    - AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY aws s3 sync --delete site s3://your-bucket-name/  # Replace with your S3 bucket name
  only:
    - master  # Deploy only on pushes to the master branch (adjust as needed)

cache:
  paths:
    - ~/.cache/pip  # Cache pip dependencies to speed up builds
</textarea>

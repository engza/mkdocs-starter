# Welcome to MkDocs

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Commands

- `brew install node` - Install node.
- `brew install mkdocs` - Install MkDocs.
- `npm install -g gulp` - Install Gulp globally.
- `mkdocs serve` - Start the live-reloading docs server.
- `mkdocs build` - Build the documentation site.
- `mkdocs -h` - Print help message and exit.
- `npm install` - Install npm packages for this project.
- `gulp` - Auto-compile SASS and JS on change.

## Project layout

```
├── README.md           # Repository readme file.
├── docs
│   ├── file            # File directory (PDFs, Word Docs, Excel Spreadsheets, etc).
│   │   └── gitlab-ci-yml.md
│   ├── img             # Image directory.
│   │   ├── favicon.ico
│   │   └── gitlab-ci-yml.jpg
│   ├── index.md        # The documentation homepage.
│   └── ...             # Other markdown pages, images and other files.
├── gulpfile.js
├── mkdocs.yml          # The MkDocs configuration file.
├── package-lock.json
├── package.json
└── src
    ├── js
    │   └── script.js   # Pre-compiled custom JS file.
    └── scss
        └── style.scss  # Pre-compiled custom SASS file.
```

## Build and Deploy Docs

<small>Source: <https://g.co/gemini/share/b46164966ebe></small>

Here's a comprehensive guide that incorporates best practices and addresses potential issues when setting up a GitLab CI/CD pipeline to build and deploy your MkDocs project's compiled files to AWS S3.

### Prerequisites

-   An existing GitLab project for your mkdocs documentation.
-   An AWS S3 bucket created for storing the static HTML files.
-   IAM user credentials with programmatic access to S3 (access key ID and secret access key).

### Steps

1.  Configure AWS Credentials (Securely):
    -   Avoid storing credentials directly in the `.gitlab-ci.yml` file. This is a security risk.
    -   Use GitLab project variables to store your AWS credentials securely. Go to your GitLab project's Settings -> CI/CD -> Variables.
    -   Create two project variables:
        -   `AWS_ACCESS_KEY_ID`: Enter your AWS access key ID.
        -   `AWS_SECRET_ACCESS_KEY`: Enter your AWS secret access key.
2.  Create the `.gitlab-ci.yml` File:
    -   In your mkdocs project's root directory, create a new file named `.gitlab-ci.yml`.
    -   Paste the following YAML code, replacing the placeholders with your values:<br>[![.gitlab-ci.yml](./img/gitlab-ci-yml.jpg)](./file/gitlab-ci-yml.md)<br><a href="#download-gitlab-ci-yml" id="download-gitlab-ci-yml">Download Yaml File</a>
3.  Explanation:
    -   `image`: This specifies the Docker image used by the GitLab Runner to build and deploy your project. You can use `python:3.9-slim` or another Python image compatible with mkdocs.
    -   `stages`: This defines the pipeline stages: `build` and `deploy`.
    -   `build`:
        -   Installs mkdocs using `pip`.
        -   Runs `mkdocs build` to generate the HTML documentation.
        -   Defines `artifacts` to capture the build output (`site` directory by default) for deployment.
    -   `deploy`:
        -   Accesses the AWS credentials securely from project variables using environment variables.
        -   Uses the AWS CLI (`aws`) to synchronize the `site` directory with your S3 bucket using `aws s3 sync`. The `--delete` flag ensures outdated files are removed from S3.
        -   Runs only on pushes to the `master` branch (customize the `only` directive as needed).
    -   `cache`: This helps speed up builds by caching pip dependencies in the `~/.cache/pip` directory.
4.  Commit and Push:
    -   Commit your changes, including the `.gitlab-ci.yml` file.
    -   Push the changes to your GitLab repository.
5.  Run the Pipeline (Optional):
    -   If you want to manually trigger the pipeline before pushing, go to your GitLab project's CI/CD tab and click Run Pipelines.

### Additional Considerations

-   Customize Output Directory: If your mkdocs output directory differs from `site`, update the `paths` in the `artifacts` section and the `aws s3 sync` command accordingly.
-   Advanced Deployment Strategies:
    -   Consider using a separate Docker image for deployment, especially if it requires different dependencies from the build image.
    -   Explore environment variables for finer-grained control over deployment settings.
-   Security:
    -   Regularly rotate your AWS credentials for enhanced security.
    -   Consider using IAM roles for service accounts instead of IAM users with static credentials (more advanced setup).
-   Error Handling:
    -   Add error handling (e.g., using conditional statements) to your `deploy` script to handle potential failures gracefully.
-   Advanced Features:
    -   Leverage GitLab Runners for faster and more scalable build execution.
    -   Use environment variables to manage different deployment environments.
    -   Integrate unit tests or linting for a more robust pipeline

<textarea id="gitlab-ci-yml" class="d-none">
image: python:3.9-slim  # Adjust Python version if needed

stages:
  - build
  - deploy

build:
  stage: build
  script:
    - pip install mkdocs
    - mkdocs build  # Build the mkdocs documentation
  artifacts:
    paths:
      - site  # Replace with your mkdocs output directory if different

deploy:
  stage: deploy
  script:
    - AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY aws s3 sync --delete site s3://your-bucket-name/  # Replace with your S3 bucket name
  only:
    - master  # Deploy only on pushes to the master branch (adjust as needed)

cache:
  paths:
    - ~/.cache/pip  # Cache pip dependencies to speed up builds
</textarea>
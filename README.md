# Drupal Documentation

## MkDocs

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

### Commands

- `brew install node` - Install node.
- `brew install mkdocs` - Install MkDocs.
- `npm install -g gulp` - Install Gulp globally.
- `mkdocs serve` - Start the live-reloading docs server.
- `mkdocs build` - Build the documentation site.
- `mkdocs -h` - Print help message and exit.
- `npm install` - Install npm packages for this project.
- `gulp` - Auto-compile SASS and JS on change.

### Project layout

```
├── README.md           # Repository readme file.
├── docs
│   ├── file            # File directory (PDFs, Word Docs, Excel Spreadsheets, etc).
│   │   └── gitlab-ci-yml.md
│   ├── img             # Image directory.
│   │   ├── favicon.ico
│   │   └── gitlab-ci-yml.jpg
│   ├── index.md        # The documentation homepage.
│   └── ...             # Other markdown pages, images and other files.
├── gulpfile.js
├── mkdocs.yml          # The MkDocs configuration file.
├── package-lock.json
├── package.json
└── src
    ├── js
    │   └── script.js   # Pre-compiled custom JS file.
    └── scss
        └── style.scss  # Pre-compiled custom SASS file.
```

***

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.stellahealth.net/bcbsmn/drupal/drupal-documentation/-/settings/integrations)

## Collaborate with your team

- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
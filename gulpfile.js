const { src, dest, watch } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const terser = require('gulp-terser');

// Define paths for source and destination files
const srcScss = 'src/scss/**/*.scss';
const destCss = 'docs/css/';
const srcJs = 'src/js/**/*.js';
const destJs = 'docs/js/';

// Compile SCSS task
function compileScss() {
  return src(srcScss)
    .pipe(sass({ outputStyle: 'compressed' })) // Set output style (compressed, expanded, etc.)
    .pipe(dest(destCss));
}

// Minify JavaScript task
function minifyJs() {
  return src(srcJs)
    .pipe(terser()) // Minify and compress JavaScript
    .pipe(dest(destJs));
}

// Watch task for automatic compilation and minification
function watchTasks() {
  watch(srcScss, compileScss);
  watch(srcJs, minifyJs);
}

// Define default task (can be changed)
exports.default = watchTasks;
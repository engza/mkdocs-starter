/**
 * Downloads content on button click.
 * @fileType (string): MIME type (e.g. "text/plain", "text/yaml")
 * @fileName (string): Desired filename for download
 * @contentId (string): ID of element containing text content to download
 * Note:
 *   Requires a button with ID in the following format: download-@contentId
 * Javascript example:
 *   downloadFile('text/plain', 'data.txt', 'content-div');
 * Button ID example:
 *   <a href="#" id="download-content-div" class="btn">Download</a>
 * Content element example:
 *   <textarea id="content-div" class="d-none"><!-- content here --></textarea>
 */
function downloadFile(fileType, fileName, contentId) {
  const button = document.getElementById("download-"+contentId);
  const contentElement = document.getElementById(contentId);
  if (!button || !contentElement) return; // Early return if button doesn't exist
  const downloadLink = document.createElement('a');
  button.addEventListener('click', () => {
    const blob = new Blob([contentElement.textContent], { type: fileType });
    const url = URL.createObjectURL(blob);
    downloadLink.href = url;
    downloadLink.download = fileName;
    downloadLink.click();
    URL.revokeObjectURL(url);
  });
}

document.addEventListener('DOMContentLoaded', () => {
  document.querySelector('footer').classList.add('footer');
  if (document.getElementById('gitlab-ci-yml')) {
    downloadFile('text/yaml', '.gitlab-ci.yml', 'gitlab-ci-yml');
  }
});